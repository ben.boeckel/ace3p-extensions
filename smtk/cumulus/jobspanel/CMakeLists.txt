include_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_AUTOUIC_SEARCH_PATHS
  "${CMAKE_CURRENT_SOURCE_DIR}/ui"
)

set(ui_files
  ui/jobtablewidget.ui
  ui/mainwindow.ui
  ui/logindialog.ui
  ui/cumuluswidget.ui
)

set(src
  jobtablewidget.cxx
  jobview.cxx
  logindialog.cxx
  jobtablemodel.cxx
  job.cxx
  cumulusproxy.cxx
  girderrequest.cxx
  jobrequest.cxx
  utils.cxx
  cumuluswidget.cxx
  mainwindow.cxx
  cJSON.c
)

#add moc files
set(mochdrs
  cumulusproxy.h
  girderrequest.h
  jobrequest.h
  jobtablemodel.h
  jobtablewidget.h
  jobview.h
  mainwindow.h
)

set(CMAKE_AUTOUIC 1)
set(CMAKE_AUTOMOC 1)
set(CMAKE_AUTORCC 1)

#install headers
set(hdrs
  cumuluswidget.h
  job.h
  logindialog.h
  cJSON.h
)
#extension library
add_library(smtkCumulus
 ${src}
 ${hdrs}
 ${mochdrs}
 ${ui_files}
 ${resources}
)
smtk_public_headers(smtkCumulus ${hdrs})

#we need to add the location of the moc files to the include dir
target_include_directories(smtkCumulus PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_include_directories(smtkCumulus PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

#publicly link to smtkCore
target_link_libraries(smtkCumulus LINK_PUBLIC
  smtkNewt
  smtkCore
  Qt5::Core
  Qt5::Gui
  Qt5::Network
  Qt5::Widgets
)
smtk_export_header(smtkCumulus Exports.h)

#install the library and exports
smtk_install_library(smtkCumulus)

#standalone cumulus executable
add_executable(cumulus MACOSX_BUNDLE
  main.cxx
)

set_target_properties(cumulus PROPERTIES AUTOMOC TRUE)

target_link_libraries(cumulus
  smtkCumulus
)
