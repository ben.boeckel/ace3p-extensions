//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/Metadata.h"

#include "smtk/resource/Resource.h"

#include <iostream>

namespace
{
typedef smtk::resource::String SMTKStringType;
typedef smtk::resource::Integer SMTKIntType;

const SMTKStringType METADATA_FORMAT_KEY = "ace3p.format";
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

// Static constants
const std::string Metadata::PROJECT_TYPENAME = "smtk::simulation::ace3p::Project";
const std::string Metadata::PROJECT_FILE_EXTENSION = ".project.smtk";
const std::string Metadata::METADATA_PROPERTY_KEY = "ace3p";
const std::string Metadata::ANALYSIS_MESH_ROLE = "analysis-mesh";
std::string Metadata::WORKFLOWS_DIRECTORY = ""; // must be initalized by application

bool Metadata::getFromResource(smtk::resource::ConstResourcePtr resource)
{
  bool isace3p = resource->properties().contains<SMTKIntType>(METADATA_FORMAT_KEY);
  if (!isace3p)
  {
    std::cerr << __FILE__ << ":" << __LINE__ << " Resource missing ace3p-formatted metadata."
              << std::endl;
    return false;
  }
  this->Format = resource->properties().at<SMTKIntType>(METADATA_FORMAT_KEY);
  if ((this->Format < 1) || (this->Format > METADATA_FORMAT_NUMBER))
  {
    std::cerr << __FILE__ << ":" << __LINE__ << " Unsupported ace3p.format " << this->Format
              << std::endl;
    return false;
  }

  const std::string prefix = std::string(Metadata::METADATA_PROPERTY_KEY) + ".";
  const std::string amKey = prefix + Metadata::ANALYSIS_MESH_ROLE;
  this->NativeAnalysisMeshLocation = resource->properties().at<SMTKStringType>(amKey);

  return true;
}

void Metadata::putToResource(smtk::resource::ResourcePtr resource)
{
  // Store instance data as POD strings
  resource->properties().insert<SMTKIntType>(METADATA_FORMAT_KEY, this->Format);
  const std::string prefix = Metadata::METADATA_PROPERTY_KEY + ".";
  const std::string amKey = prefix + Metadata::ANALYSIS_MESH_ROLE;
  resource->properties().insert<SMTKStringType>(amKey, this->NativeAnalysisMeshLocation);
}

}
}
}
