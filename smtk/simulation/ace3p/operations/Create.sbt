<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the ACE3P Project "Create" Operation -->
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <!-- Parameters -->
    <AttDef Type="create" Label="Project - Create" BaseType="operation">
      <BriefDescription>
        Create a CMB project for preparing Truchas simulation inputs.
      </BriefDescription>

      <ItemDefinitions>
        <Directory Name="location" Label="Project Directory" NumberOfRequiredValues="1">
          <BriefDescription>The folder in the local filesystem for storing project files.</BriefDescription>
        </Directory>
        <Void Name="overwrite" Label="OK to overwrite existing directory?" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Flag indicating whether or not it OK to erase the contents of project folder if it is not empty.</BriefDescription>
        </Void>
        <String Name="analysis-name" Label="Analysis Name">
          <BriefDescription>The label used to identify the analysis-specification resource.</BriefDescription>
          <DetailedDescription>The label used to identify the analysis-specification resource.
            This is for convenience when multiple analyses are included in the same project.
          </DetailedDescription>
          <DefaultValue>Analysis1</DefaultValue>
        </String>
        <File Name="analysis-mesh" Label="Analysis Mesh" Optional="true"
          IsEnabledByDefault="true" NumberOfRequiredValues="1" ShouldExist="true"
          FileFilters="Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)">
          <BriefDescription>The input file to be used as the heat transfer mesh.</BriefDescription>
        </File>
        <Void Name="copy-file" Label="Copy Geometry File(s) Into Project Folder" AdvanceLevel="1" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>If enabled, store a copy of the geometry file in the project directory.</BriefDescription>
        </Void>
        <File Name="simulation-template" Label="Override Simulation Template" AdvanceLevel="1" ShouldExist="true" Optional="true" IsEnabledByDefault="false" FileFilters="CMB Template Files (*.sbt);;All Files (*)">
          <BriefDescription>The simulation template to use in place of the default.</BriefDescription>
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(create)" BaseType="result">
      <ItemDefinitions>

        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::simulation::ace3p::Project"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Operation" Title="New Project" TopLevel="true" FilterByAdvanceLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="create" Type="create"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
