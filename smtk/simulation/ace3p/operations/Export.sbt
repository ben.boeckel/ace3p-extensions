<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the ACE3P Project "Export" Operation -->
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Rf-Postprocess</Cat>
  </Categories>

  <Definitions>
    <!-- Parameters -->
    <include href="smtk/operation/Operation.xml"/>
    <!-- ace3p-submit is in simulation-workflows/internal -->
    <include href="ace3p-submit.xml" />
    <AttDef Type="ace3p-export" BaseType="operation" Label="Export to ACE3P">
      <BriefDescription>
        Write ACE3P input file.
      </BriefDescription>
      <DetailedDescription>
        Using the specified analysis, this operation writes a ACE3P
        input file to the file system.
      </DetailedDescription>

      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="Read" Extensible="false" OnlyResources="true"
                       AdvanceLevel="1">
        <Accepts>
          <Resource Name="smtk::simulation::ace3p::Project"/>
          <!-- Accept *any* project as temp workaround -->
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Resource Name="analysis" Label="Analysis" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" />
          </Accepts>
        </Resource>
        <Directory Name="OutputFolder" Label="Export Folder" Version="0">
          <BriefDescription>The folder to use on the local filesystem</BriefDescription>
        </Directory>
        <String Name="OutputFilePrefix" Label="Output File Prefix" Version="0">
          <BriefDescription>The prefix to use for generated files</BriefDescription>
        </String>
        <File Name="MeshFile" Label="Output Mesh" Version="0">
          <BriefDescription>The mesh</BriefDescription>
        </File>
        <Void Name="test-mode" Label="Test Mode" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>For internal use</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(export-ace3p)" BaseType="result">
      <ItemDefinitions>
        <File Name="output-file" />
        <String Name="CumulusJobId" AdvanceLevel="99" Optional="true" IsEnabledByDefault="false" />
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Operation" Title="Export ACE3P File" TopLevel="true" FilterByAdvanceLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="ace3p-export" Type="ace3p-export"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
