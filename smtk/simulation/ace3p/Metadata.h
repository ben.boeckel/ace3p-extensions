//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_ace3p_Metadata_h
#define __smtk_simulation_ace3p_Metadata_h

#include "smtk/simulation/ace3p/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/resource/PropertyType.h"

#include "nlohmann/json.hpp"

#include <string>

namespace
{
const smtk::resource::Integer METADATA_FORMAT_NUMBER = 1;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/**\brief Metadata specific to ace3p projects.
 *
 * Includes both static constants used to across the code base and
 * instance data that are stored in the resource properties on the
 * project instance.
 */

struct SMTKACE3P_EXPORT Metadata
{
  // Static constants - initialized in the cxx file
  static const std::string PROJECT_TYPENAME;            // "ace3p"
  static const std::string PROJECT_FILE_EXTENSION;      // ".project.smtk";
  static const std::string METADATA_PROPERTY_KEY;       // "ace3p";
  static const std::string ANALYSIS_MESH_ROLE;     // "analysis-mesh"

  // Directory containing ACE3P.sbt and ACE3P.py
  // Applications must set this!
  static std::string WORKFLOWS_DIRECTORY;

  // Interim methods to add and retrieve instance data from project
  //   getFromResource() returns boolean indicating whether or not the resource
  //   is formatted with ACE3P metadata.
  bool getFromResource(smtk::resource::ConstResourcePtr resource);
  void putToResource(smtk::resource::ResourcePtr resource);

  // Instance data - stored in the project resource properties
  smtk::resource::Integer Format = METADATA_FORMAT_NUMBER;
  smtk::resource::String NativeAnalysisMeshLocation;
};

}
}
}

#endif
