//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/ace3p/Project.h"

#include <iostream>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

Project::Project()
{
  // std::cout << "ace3p Project Constructor!" << std::hex << this << std::endl;
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
