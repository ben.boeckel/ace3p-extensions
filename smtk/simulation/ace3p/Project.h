//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_Project_h
#define smtk_simulation_ace3p_Project_h

#include "smtk/project/Project.h"

#include "smtk/simulation/ace3p/DerivedFrom.h"
#include "smtk/simulation/ace3p/Exports.h"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3P_EXPORT Project // : public smtk::project::Project
  : public DerivedFrom<Project, smtk::project::Project>
{
public:
  smtkTypeMacro(smtk::simulation::ace3p::Project);
  smtkCreateMacro(smtk::project::Project);

  virtual ~Project() {}

protected:
  Project();
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
