//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// Static var to store the singleton instance
static qtProjectRuntime* g_instance = nullptr;

qtProjectRuntime* qtProjectRuntime::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new qtProjectRuntime(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

qtProjectRuntime::~qtProjectRuntime()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
