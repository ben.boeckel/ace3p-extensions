//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNerscFileItem.h Custom smtk item for NERSC filesystem paths.
// .SECTION Description
// .SECTION See Also

#ifndef smtk_simulation_ace3p_qt_qtNerscFileItem
#define smtk_simulation_ace3p_qt_qtNerscFileItem

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/extension/qt/qtAttributeItemInfo.h"
#include "smtk/extension/qt/qtItem.h"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtNerscFileItem : public smtk::extension::qtItem
{
  Q_OBJECT
  using Superclass = smtk::extension::qtItem;

public:
  static smtk::extension::qtItem* createItemWidget(
    const smtk::extension::qtAttributeItemInfo& info);
  qtNerscFileItem(const smtk::extension::qtAttributeItemInfo& info);
  virtual ~qtNerscFileItem();

signals:

public slots:

protected slots:
  void onBrowse();
  /// Updates UI and StringItem with path sent from external dialog
  void onDialogApply(const QString& path);
  // Updates StringItem from user input in QLineEdit
  void onEditingFinished();

protected:
  void clearChildWidgets();
  void createWidget();
  void setBackground();
  void updateUI();

private:
  class Internal;
  Internal* m_internal;
};
}
}
} // end namespace

#endif
