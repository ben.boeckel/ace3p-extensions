//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PSaveBehavior_h
#define pqACE3PSaveBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqACE3PSaveReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PSaveReaction(QAction* parent);

  void saveProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->saveProject(); }

private:
  Q_DISABLE_COPY(pqACE3PSaveReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3PSaveBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PSaveBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PSaveBehavior() override;

protected:
  pqACE3PSaveBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqACE3PSaveBehavior);
};

#endif
