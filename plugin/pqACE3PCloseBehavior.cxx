//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PCloseBehavior.h"

#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// // Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

#include <vector>

#define NOT_WORKING 0
#if NOT_WORKING
#include "smtk/common/Processing.h"
#include "smtk/resource/Manager.h"
#endif

//-----------------------------------------------------------------------------
pqACE3PCloseReaction::pqACE3PCloseReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqACE3PCloseReaction::onTriggered()
{
  pqACE3PCloseBehavior::instance()->closeProject();
}


//-----------------------------------------------------------------------------
static pqACE3PCloseBehavior* g_instance = nullptr;

//-----------------------------------------------------------------------------
pqACE3PCloseBehavior::pqACE3PCloseBehavior(QObject* parent)
  : Superclass(parent)
{
}

//-----------------------------------------------------------------------------
pqACE3PCloseBehavior* pqACE3PCloseBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3PCloseBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

//-----------------------------------------------------------------------------
pqACE3PCloseBehavior::~pqACE3PCloseBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

//-----------------------------------------------------------------------------
void pqACE3PCloseBehavior::closeProject()
{
  // Get current project
  auto project = qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Internal error - no active project.";
    return;
  }

  // Check if project is modified
  if (!project->clean())
  {
    qInfo() << "TODO project is modified.";
  }

  // Access the active server and get the project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
#if NOT_WORKING
  auto resManager = wrapper->smtkResourceManager();
  resManager->visit([](smtk::resource::Resource& res) {
    qInfo() << "Resource" << res.typeName().c_str() << ", name" << res.name().c_str() << "Clean?"
            << res.clean();
    return smtk::common::Processing::CONTINUE;
  });
  QString msg = "This function is disabled because SMTK LOCKS UP"
                " when removing the individual resources from the resource manager"
                " (wish I knew why). All you can do is close & restart modelbuilder.";
  QMessageBox::information(pqCoreUtilities::mainWidget(), "Not Implemented", msg);
  return;
#else
  auto projectName = project->name();
  auto resManager = std::static_pointer_cast<smtk::resource::Resource>(project)->manager();

  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    auto resource = *iter;
    resManager->remove(resource);
  }

  // Must remove project from *both* resource manager & project manager
  resManager->remove(project);
  projectManager->remove(project);

  qInfo() << "Closed project" << projectName.c_str();
  emit this->projectClosed();
#endif
} // closeProject()
