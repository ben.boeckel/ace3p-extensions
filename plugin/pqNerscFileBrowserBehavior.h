//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqNerscFileBrowserBehavior_h
#define pqNerscFileBrowserBehavior_h

// ACE3P Extensions includes
#include "smtk/newt/qtNewtLoginDialog.h"

// ParaView includes
#include "pqReaction.h"

// Qt includes
#include <QObject>

namespace newt
{
class qtNewtFileBrowserWidget;
}

//\brief A reaction for showing the NERSC file browser widget
class pqNerscFileBrowserReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqNerscFileBrowserReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqNerscFileBrowserReaction)
};

/** \brief Add a menu item for opening the NERSC file browser
  */
class pqNerscFileBrowserBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqNerscFileBrowserBehavior* instance(QObject* parent = nullptr);
  ~pqNerscFileBrowserBehavior() override;

  // Opens or raises the NERSC file browser
  void showBrowser();

protected:
  pqNerscFileBrowserBehavior(QObject* parent = nullptr);

  newt::qtNewtFileBrowserWidget* m_browserWidget;

private:
  Q_DISABLE_COPY(pqNerscFileBrowserBehavior);
};

#endif
