//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqACE3PAutoStart_h
#define plugin_pqACE3PAutoStart_h

#include <QObject>

class pqServer;
class pqSMTKWrapper;

class pqACE3PAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqACE3PAutoStart(QObject* parent = nullptr);
  ~pqACE3PAutoStart() = default;

  void startup();
  void shutdown();

protected slots:
  void resourceManagerAdded(pqSMTKWrapper* mgr, pqServer* server);
  void resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server);

private:
  Q_DISABLE_COPY(pqACE3PAutoStart);
};

#endif
