//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PLoader_h
#define pqACE3PLoader_h

#include "smtk/PublicPointerDefs.h"

#include <QObject>

class pqServer;
class pqServerResource;

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqACE3PLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqACE3PLoader* instance(QObject* parent = nullptr);
  ~pqACE3PLoader() override;

  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer* server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

signals:
  void projectOpened(smtk::project::ProjectPtr);

protected:
  pqACE3PLoader(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqACE3PLoader);
};

#endif // pqACE3PLoader_h
