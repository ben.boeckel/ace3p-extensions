//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PMenu.h"

#include "pqNerscFileBrowserBehavior.h"
#include "pqNerscLoginBehavior.h"
#include "pqACE3PNewBehavior.h"
#include "pqACE3PExportBehavior.h"
#include "pqACE3PSaveBehavior.h"
#include "pqACE3POpenBehavior.h"
#include "pqACE3PCloseBehavior.h"
#include "pqACE3PRecentProjectsMenu.h"
#include "pqACE3PLoader.h"

#include "smtk/newt/qtNewtInterface.h"

// ParaView includes
#include "pqApplicationCore.h"

// Qt includes
#include <QAction>
#include <QDebug>
#include <QMenu>
#include <QString>

//-----------------------------------------------------------------------------
pqACE3PMenu::pqACE3PMenu(QObject* parent)
  : Superclass(parent)
  , m_nerscBrowserAction(nullptr)
  , m_nerscLoginAction(nullptr)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqACE3PMenu::~pqACE3PMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqACE3PMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize ACE3P menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto openProjectBehavior = pqACE3POpenBehavior::instance(this);
  auto newProjectBehavior = pqACE3PNewBehavior::instance(this);
  auto saveProjectBehavior = pqACE3PSaveBehavior::instance(this);
  auto exportProjectBehavior = pqACE3PExportBehavior::instance(this);
  auto nerscLoginBehavior = pqNerscLoginBehavior::instance(this);
  auto closeProjectBehavior = pqACE3PCloseBehavior::instance(this);

  QObject::connect(newProjectBehavior, &pqACE3PNewBehavior::projectCreated, this,
    &pqACE3PMenu::onProjectOpened);

  // Initialize Open Project action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction = new pqACE3POpenReaction(m_openProjectAction);
  auto startIcon = QIcon(":/icons/toolbar/openProject.svg");
  m_openProjectAction->setIcon(startIcon);
  m_openProjectAction->setIconVisibleInMenu(true);

  // Initialize Recent Projects menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* menu = new QMenu();
  m_recentProjectsAction->setMenu(menu);
  m_recentProjectsMenu = new pqACE3PRecentProjectsMenu(menu, menu);

  // Initialize New Project action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new pqACE3PNewReaction(m_newProjectAction);
  auto newIcon = QIcon(":/icons/toolbar/newProject.svg");
  m_newProjectAction->setIcon(newIcon);
  m_newProjectAction->setIconVisibleInMenu(true);

  // Initialize Save Project action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction = new pqACE3PSaveReaction(m_saveProjectAction);
  auto saveIcon = QIcon(":/icons/toolbar/saveProject.svg");
  m_saveProjectAction->setIcon(saveIcon);
  m_saveProjectAction->setIconVisibleInMenu(true);

  // Initialize Export Project action
  m_exportProjectAction = new QAction(tr("Export Analysis..."), this);
  auto exportProjectReaction = new pqACE3PExportReaction(m_exportProjectAction);
  auto exportIcon = QIcon(":/icons/toolbar/exportProject.svg");
  m_exportProjectAction->setIcon(exportIcon);
  m_exportProjectAction->setIconVisibleInMenu(true);

  // Initialize NERSC login action
  m_nerscLoginAction = new QAction(tr("Login to NERSC"), this);
  auto nerscLoginReaction = new pqNerscLoginReaction(m_nerscLoginAction);
  auto nerscIcon = QIcon(":/icons/toolbar/nersc.svg");
  m_nerscLoginAction->setIcon(nerscIcon);
  m_nerscLoginAction->setIconVisibleInMenu(true);

  // Initialize Close Project action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction = new pqACE3PCloseReaction(m_closeProjectAction);
  auto closeIcon = QIcon(":/icons/toolbar/closeProject.svg");
  m_closeProjectAction->setIcon(closeIcon);
  m_closeProjectAction->setIconVisibleInMenu(true);
#if 0
  // File browser menu is for dev only
  auto nerscBrowserBehavior = pqNerscFileBrowserBehavior::instance(this);

  m_nerscBrowserAction = new QAction(tr("NERSC File Browser"), this);
  m_nerscBrowserAction->setEnabled(false);
  auto nerscBrowserReaction = new pqNerscFileBrowserReaction(m_nerscBrowserAction);

  // Enable rest of menu when on login signal
  auto newtInterface = newt::qtNewtInterface::instance();
  QObject::connect(newtInterface, &newt::qtNewtInterface::loginComplete,
    [this]() { m_nerscBrowserAction->setEnabled(true); });
#endif


  QObject::connect(
    closeProjectBehavior, &pqACE3PCloseBehavior::projectClosed,
    this,                 &pqACE3PMenu::onProjectClosed
  );

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  auto projectLoader = pqACE3PLoader::instance();
  QObject::connect(
    projectLoader, &pqACE3PLoader::projectOpened,
    this,          &pqACE3PMenu::onProjectOpened
  );
  return true;
}

void pqACE3PMenu::shutdown()
{
}

//-----------------------------------------------------------------------------
void pqACE3PMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  // m_saveAsProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);

  // auto secondModel = project->findResource<smtk::model::Resource>("second");
  // m_importModelAction->setEnabled(secondModel == nullptr);
}

//-----------------------------------------------------------------------------
void pqACE3PMenu::onProjectClosed()
{
  m_newProjectAction->setEnabled(true);
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(true);
  m_saveProjectAction->setEnabled(false);
  // m_saveAsProjectAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_exportProjectAction->setEnabled(false);
  // m_importModelAction->setEnabled(false);
}
