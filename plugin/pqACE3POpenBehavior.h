//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3POpenBehavior_h
#define pqACE3POpenBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqACE3POpenReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3POpenReaction(QAction* parent);

  void openProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->openProject(); }

private:
  Q_DISABLE_COPY(pqACE3POpenReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3POpenBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3POpenBehavior* instance(QObject* parent = nullptr);
  ~pqACE3POpenBehavior() override;

protected:
  pqACE3POpenBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqACE3POpenBehavior);
};

#endif
