//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugin_pqACE3PPluginLocation_h
#define plugin_pqACE3PPluginLocation_h

#include <QObject>

class pqACE3PPluginLocation : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqACE3PPluginLocation(QObject* parent = nullptr);
  ~pqACE3PPluginLocation() override;

  void StoreLocation(const char* location);

private:
  Q_DISABLE_COPY(pqACE3PPluginLocation);
};

#endif
