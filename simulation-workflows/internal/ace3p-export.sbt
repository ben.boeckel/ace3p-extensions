<?xml version="1.0"?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Simulation</Cat>
    <Cat>Rf-Postprocess</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="ace3p-export" BaseType="operation" Label="Export to ACE3P" Version="2">
      <Categories>
        <Cat>Simulation</Cat>
      </Categories>
      <!-- Version 2 adds option for NEWT Session ID -->
      <BriefDescription>
        Write ACE3P input file for selected program.
      </BriefDescription>
      <DetailedDescription>
      </DetailedDescription>
      <ItemDefinitions>
        <Resource Name="model" Label="Model" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" />
          </Accepts>
        </Resource>
        <Resource Name="attributes" Label="Attributes" LockType="DoNotLock">
          <Categories>
            <Cat>Rf-Postprocess</Cat>
          </Categories>
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <File Name="MeshFile" Label="Mesh File" ShouldExist="True"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;NetCDf files (*.ncdf);; All files (*.*)" Version="0">
          <CategoryInfo Combination="All">
            <Exclude Combination="All">
              <Cat>Rf-Postprocess</Cat>
            </Exclude>
          </CategoryInfo>
        </File>
        <Directory Name="OutputFolder" Label="Export Folder" Version="0">
          <BriefDescription>The folder to use on the local filesystem</BriefDescription>
          <Categories>
            <Cat>Rf-Postprocess</Cat>
          </Categories>
        </Directory>
        <String Name="OutputFilePrefix" Label="Filename prefix" Version="0">
          <BriefDescription>The prefix to use for generated files</BriefDescription>
          <Categories>
            <Cat>Rf-Postprocess</Cat>
          </Categories>
        </String>
        <Void Name="TestMode" Label="Test Mode" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>For internal use</BriefDescription>
          <Categories>
            <Cat>Rf-Postprocess</Cat>
          </Categories>
        </Void>

        <!-- ACE3P.py inserts <Group Name="NERSCSimulation> here -->
      </ItemDefinitions>
     </AttDef>

     <AttDef Type="export-result" BaseType="result">
      <ItemDefinitions>
        <File Name="output-file" />
        <String Name="CumulusJobId" AdvanceLevel="99" Optional="true" IsEnabledByDefault="false" />
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  <Views>
    <View Type="Operation" Title="Export Settings" TopLevel="true" FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="ace3p-export" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
